import enum


class ExtEnum(enum.Enum):
    GPX = "gpx"
    KML = "kml"
    GEOJSON = "geojson"
    MAP = "map"
